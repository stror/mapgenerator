﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoordinateCalculate
{
    public class Functions
    {
        /// <summary>
        /// Массив содержит количество пикселей в зависимости от уровня зума
        /// </summary>
        double pixelGlobeSize;


        //В конструкторе вычисляем количество пикселей (всеё планеты) для каждого из масштабов (Zoom). 
        public Functions(int zoomLevel)
        {
            pixelGlobeSize = 256 * Math.Pow(2, zoomLevel);
        }

        /// <summary>
        /// Вычисление индекса пикселя по горизонтали на основе текущей долготы
        /// </summary>
        /// <param name="Lon">Текущая долгота</param>
        /// <param name="Zoom">Уровень зума, указанный в запросе к googleApi</param>
        /// <returns></returns>
        public double GoogleLon2BmpPix(double Lon)
        {
            return (pixelGlobeSize / 2 + Lon * pixelGlobeSize / 360);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x">Горизонтальный индекс пикселя</param>
        /// <param name="Zoom">Уровень зума, указанный в запросе к googleApi</param>
        /// <returns></returns>
        public double GoogleBmpPix2Lon(double x)
        {
            return (x - pixelGlobeSize / 2) / (pixelGlobeSize / 360);
        }

        /// <summary>
        /// Вычисление индекса пикселя по вертикали на основе текущей широты
        /// </summary>
        /// <param name="Lat">Текущая широта</param>
        /// <param name="Zoom">Уровень зума, указанный в запросе к googleApi</param>
        /// <returns></returns>
        public double GoogleLat2BmpPix(double Lat)
        {
            double k = Math.Sin(Lat * Math.PI / 180);
            return (pixelGlobeSize / 2 - 0.5 * Math.Log((1 + k) / (1 - k)) * pixelGlobeSize / (2 * Math.PI));
        }

        /// <summary>
        /// Вычисление широты по вертикальному индексу пикселя и уровню зума
        /// </summary>
        /// <param name="y">Вертикальный индекс пикселя</param>
        /// <param name="Zoom">Уровень зума, указанный в запросе к googleApi</param>
        /// <returns></returns>
        public double GoogleBmpPix2Lat(double y)
        {
            double k = (y - pixelGlobeSize / 2) / -(pixelGlobeSize / (2 * Math.PI));
            return (2 * Math.Atan(Math.Exp(k)) - Math.PI / 2) * 180 / Math.PI;
        }

        /// <summary>
        /// Вычисление долготы на основе текущей долготы и сдвига по горизонтали в пикселях
        /// </summary>
        /// <param name="Longitude">Текущая долгота</param>
        /// <param name="Zoom">Уровень зума, указанный в запросе к googleApi</param>
        /// <param name="DiffW">Сдвиг по горизонтали в пикселях</param>
        /// <returns>Долгота</returns>
        public double GoogleLonPixDiff(double Longitude, int DiffW)
        {
            return GoogleBmpPix2Lon(GoogleLon2BmpPix(Longitude) + DiffW);
        }

        /// <summary>
        /// Вычисление широты на основе текущей широты и сдвига по вертикали в пикселях
        /// </summary>
        /// <param name="Latitude">Текущая широта</param>
        /// <param name="Zoom">Уровень зума, указанный в запросе к googleApi</param>
        /// <param name="DiffH">Сдвиг по вертикали в пикселях</param>
        /// <returns>Широта</returns>
        public double GoogleLatPixDiff(double Latitude, int DiffH)
        {
            return GoogleBmpPix2Lat(GoogleLat2BmpPix(Latitude) + DiffH);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoordinateCalculate
{
    /// <summary>
    /// Генератора карты выдаёт значения центров всех квадратов в широте и долготе
    /// </summary>
    public class MapGenerator
    {
        public int Zoom { get; set; } 
        public int SizeW { get; set; }  
        public int SizeH { get; set; }  
        public Coordinate StartCoordinate { get; set; }
        public int MapSize { get; set; }

        private Functions functions;

        /// <summary>
        /// Конструктор генератора
        /// </summary>
        /// <param name="Zoom">Уровень зума, указанный в googleApi</param>
        /// <param name="SizeW">Размер прямоугольника по ширине</param>
        /// <param name="SizeH">Размер прямоугольника по высоте</param>
        /// <param name="StartCoordinate">Начальные координаты. Считаются с левого верхнего угла</param>
        /// <param name="MapSize">Размер генерируемой карты. Пример: при вводе значения 3 - 3х3</param>
        public MapGenerator(int Zoom, int SizeW, int SizeH, Coordinate StartCoordinate, int MapSize)
        {
            this.Zoom = Zoom;
            this.SizeW = SizeW;
            this.SizeH = SizeH;
            this.StartCoordinate = StartCoordinate;
            this.MapSize = MapSize;

            functions = new Functions(Zoom);
        }

        public Coordinate[,] Generate()
        {
            Coordinate[,] CenterCoordinates = new Coordinate[MapSize, MapSize];
            double latitude = StartCoordinate.Latitude;
            double longitude = StartCoordinate.Longitude;
            CenterCoordinates[0, 0] = new Coordinate(latitude, longitude);

            for (int i = 0; i < MapSize; i++)
            {
                if(i != 0)
                    latitude = functions.GoogleLatPixDiff(latitude, SizeH);

                for (int j = 0; j < MapSize; j++)
                {
                    if (i == 0 && j == 0)
                        CenterCoordinates[i, j] = new Coordinate(latitude, longitude);
                    else if (j == 0)
                    {
                        longitude = StartCoordinate.Longitude;
                        CenterCoordinates[i, j] = new Coordinate(latitude, longitude);
                    }
                    else
                    {
                        longitude = functions.GoogleLonPixDiff(longitude, SizeW);
                        CenterCoordinates[i, j] = new Coordinate(latitude, longitude);
                    }
                }               
            }

            return CenterCoordinates;
        }

        public List<Coordinate> GenerateToList()
        {
            List<Coordinate> CenterCoordinates = new List<Coordinate>();
            double latitude = StartCoordinate.Latitude;
            double longitude = StartCoordinate.Longitude;

            for (int i = 0; i < MapSize; i++)
            {
                if (i != 0)
                    latitude = functions.GoogleLatPixDiff(latitude, SizeH);                    
                    
                for (int j = 0; j < MapSize; j++)
                {
                    if (i == 0 && j == 0)
                        CenterCoordinates.Add(new Coordinate(latitude, longitude));
                    else if(j == 0)
                    {
                        longitude = StartCoordinate.Longitude;
                        CenterCoordinates.Add(new Coordinate(latitude, longitude));
                    }                        
                    else
                    {
                        longitude = functions.GoogleLonPixDiff(longitude, SizeW);
                        CenterCoordinates.Add(new Coordinate(latitude, longitude));
                    }                        
                }
            }

            return CenterCoordinates;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeocodingAPI.Model
{
    class StaticMap
    {
        public string MapType { get; set; } //satellite
        public int Zoom { get; set; }   //15
        public int Scale { get; set; }  //2
        public int SizeW { get; set; }  //512
        public int SizeH { get; set; }  //512
        public string Format { get; set; }  //.jpg
    }
}

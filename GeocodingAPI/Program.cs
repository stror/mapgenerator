﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using GeocodingAPI.Model;
using System.IO;
using System.Threading;
using System.Drawing;
using System.Net;
using CoordinateCalculate;

namespace GeocodingAPI
{
    class Program
    {
        public static int offset = 268435456;
        public static double radius = 85445659.44705395;
        public static int LevelSize = 33554432;

        static void Main(string[] args)
        {

            string APIkey = "AIzaSyDqwVRJ43ltWeFztIMXKw5l0Td4ZIUEyY8";
            HttpClient client = HttpClientInit();
            string uri = "";
            
            StaticMap configurations = new StaticMap() {
                MapType = "satellite",
                Zoom = 17,
                Scale = 2,
                SizeH = 512,
                SizeW = 512,
                Format = "jpg"
            };

            MapGenerator generator = new MapGenerator(configurations.Zoom, configurations.SizeH, configurations.SizeW, new Coordinate(55.721999, 37.900676), 3); //инициализируем генератор
            List<Coordinate> Coordinates = generator.GenerateToList(); //генерируем координаты

            
            int i = 0; //счетчик для имен файлов

            foreach (Coordinate location in Coordinates)
            {
                uri = $"https://maps.googleapis.com/maps/api/staticmap?center={location.Latitude},{location.Longitude}&maptype={configurations.MapType}&zoom={configurations.Zoom}&scale={configurations.Scale}&size={configurations.SizeH}x{configurations.SizeW}&format={configurations.Format}&key={APIkey}";

                using (WebClient wc = new WebClient())
                {
                    wc.DownloadFile(uri, string.Format(@"c:\temp\img{0}.jpg", i));
                    i++;
                }
            }            
        }

        static HttpClient HttpClientInit()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("https://maps.googleapis.com/maps/api/");
            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }
    }
}
